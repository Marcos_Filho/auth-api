import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { isSignedIn } from "./src/auth";

import { CreateRootNavigator } from './src/routes';


export default class App extends React.Component {
  state = {
    signed: false,
    signLoaded: false,
  };

  componentDidMount() {
    isSignedIn()
      .then(res => this.setState({ signed: res, signLoaded: true }))
      .catch(err => alert("Erro"));
  }

  render() {
    const { signLoaded, signed } = this.state;

    if (!signLoaded) {
      return null;
    }

    console.log('signed: '+signed)
    return CreateRootNavigator(signed);
  }
}
