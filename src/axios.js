import axios from 'axios';

const api = axios.create({
    baseURL: 'your_URL_here'
});


export default api;